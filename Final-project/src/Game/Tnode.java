package Game;

public class Tnode extends Node {
    public Tnode parent;
    public Tnode left;
    public Tnode right;
    public double x;       //destination position
    public double y;          //destination position
    public int height;

    public Tnode(int value, Tnode parent) {
        super(400,300, value);
        this.parent = parent;
    }

    public void setDestination(double x, double y)
    {
        this.x = x;
        this.y = y;
    }
}
