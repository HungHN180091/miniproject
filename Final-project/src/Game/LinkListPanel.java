package Game;
import tklibs.SpriteUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;


import static Game.Program.clickFirst;
import static Game.Program.p;


public class LinkListPanel extends JPanel {
    // duong dan tuyet doi: /Users/ngando1720/Documents/Java OOP/ci-begin-master/assets/images/players/straight/0.png
    // duong dan tuong doi: assets/images/players/straight/0.png

    public Background background;
    public double fps;
    public Node node,node1,node2;
    public ArrayList<Node> n;
    int frameDelay = 110;
    int count=0;
    int done=0;
    int isDoing =0;
    int i;
    boolean refactor;

    BufferedImage logoImage;
    BufferedImage cover;
    BufferedImage cover2;
    BufferedImage popLogo;
    BufferedImage pushLogo;


    public LinkListPanel() {
        background = new Background();
        n = new ArrayList<Node>();  // Create an empty list of node
    }
    static Font fontStart = new Font("Verdana", Font.BOLD, 20);
    /**
     * auto been called by program
     *
     * @param g
     */
    @Override
    public void paintComponent(Graphics g) { // paint cai intro
        super.paintComponent(g);
        if (clickFirst <1) {
            background.render(g);
            logoImage = SpriteUtils.loadImage("assets/images/football/logo.png");
            g.drawImage(logoImage, 150, 200, null);
            cover = SpriteUtils.loadImage("assets/images/football/cover.png");
            g.drawImage(cover,450, 300, null  );
            cover2 = SpriteUtils.loadImage("assets/images/football/cover2.png");
            g.drawImage(cover2,30, 300, null  );
            g.setFont(fontStart);
            g.setColor(Color.white);
            g.drawString( " Click anywhere to start !!! ", 305, 860);

        } else {
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, Settings.GAME_WIDTH, Settings.GAME_HEIGHT);

            this.runAll();
//        background.render(g);
//        player.render(g);
//            System.out.println("painting ");
            GameObject.renderAll(g);
            this.drawMenu(g);
        }
    }

    static Font font = new Font("Verdana", Font.BOLD, 32);
    private void drawMenu(Graphics g) {
//        g.setColor(Color.BLACK);
//        g.fillRect(Settings.BACKGROUND_WIDTH,0,Settings.GAME_WIDTH - Settings.BACKGROUND_WIDTH, Settings.GAME_HEIGHT);
        g.setColor(Color.RED);
        g.drawString(fps + " ",750, 50);
        logoImage = SpriteUtils.loadImage("assets/images/football/logo.png");
        g.drawImage(logoImage, 150, 10, null);
//        popLogo = SpriteUtils.loadImage("assets/images/football/shoe.png");
//        g.drawImage(popLogo,300, 100,null);
        pushLogo = SpriteUtils.loadImage("assets/images/football/insertlogo.png");
        g.drawImage(pushLogo,420, 700,null); //500 100
        g.setFont(fontStart);
        g.drawString("HEAD", 50, 435);

        for (int j = 0; j < n.size(); j++) {
            g.setFont(font);
            g.setColor(Color.red);
            g.drawString( n.get(j).value+"", (int)n.get(j).position.x+20, (int)n.get(j).position.y+41);
//            g.drawString("dcm test chu", 50, 500);
        }
    }

    int frameCount=0;
    public void runAll() {
        // not necessary to make all objects run
        GameObject.runAll();

        // Add a new node
        if (p!=null && p.isPush==1) { // glove
            if (isDoing==0) {
                n.add(new Node(-50, -50));
                i = n.size() - 1;
                isDoing=1;
            }
            n.get(i).moveTo(150 + i * 100,400);
            if (n.get(i).position.x == 150 + i * 100 && n.get(i).position.y== 400) {
                p.isPush=0;
                isDoing=0;
            }
        }

        //Delete a node
        if (p!= null && p.isPop != 0) { //shoe
            i = p.isPop - 1;
            n.get(i).moveTo(-60,-60);
            if (n.get(i).position.x==-60 && n.get(i).position.y==-60) {
                p.isPop=0;
                isDoing = 0;
                refactor = true;
                n.remove(i);
            }
        }

        //move nodes after the removed node backward 1 step
        if (refactor)
        {
            for (int j = i; j < n.size(); j++) {
                n.get(j).moveTo(150 + j * 100,400);
                if (n.get(j).position.x == 150 + j * 100 && n.get(j).position.y== 400) {
                    System.out.println("PASS");
                    refactor = false;
                }
            }
        }
    }

    public void gameLoop() {
        //long ~ big int
        long lastTime = System.currentTimeMillis();

        while (true) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - lastTime >= 1000 / 60) {
                fps = 1000/ (currentTime - lastTime);
                //paint
                this.repaint();
                //runt all logic  ()
//                this.runAll();
//                System.out.println();
                lastTime = currentTime;
            }

        }
    }
}

