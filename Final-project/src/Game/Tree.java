package Game;

import java.util.*;

public class Tree {
    private Tnode root;
    private int totalNode = 0; // Temporary variable

    public int getHeight(Tnode node) {
        if (node == null) return -1;
        else {
            int max = getHeight(node.left);
            int right = getHeight(node.right);
            if (max < right)
                max = right;
            return 1 + max;
        }
    }

    public int getBalance(Tnode node)
    {
        return getHeight(node.left) - getHeight(node.right);
    }

    /*public int getMaxHeight()
    {
        return getMaxHeight(this.root);
    }*/


    public Tnode add(int value, Tnode node, Tnode parent) {
        if (node == null) {
            node = new Tnode(value, parent);
            return node;
        }

        if (value < node.value)
            node.left = add(value, node.left, node);
        else if (value > node.value)
            node.right = add(value, node.right, node);
        else {
            System.out.println("The value is already in the list");
        }
        int balance = getBalance(node);

        //Left left case
        if (balance > 1 && value < node.left.value)
            node = this.rightRotate(node);
        //Right right case
        if (balance < -1 && value > node.right.value)
            node = this.leftRotate(node);
        //Left right case
        if (balance > 1 && value > node.left.value) {
            node.left = this.leftRotate(node.left);
            node = rightRotate(node);
        }
        //Right left case
        if (balance < -1 && value < node.right.value) {
            node.right = this.rightRotate(node.right);
            node = leftRotate(node);
        }

        return node;
    }

    public void add(int value) {
        this.root = this.add(value, this.root, null);
        this.updateDestination();
        this.traversal();
    }

    public Tnode delete(int value, Tnode node, Tnode parent) {
        //Simple BST delete node
        if (node == null)
            return node;

        if (value < node.value)
            node.left = delete(value, node.left, node);
        else if (value > node.value)
            node.right = delete(value, node.right, node);
        else {
            //one or no child
            if ((node.left == null) || (node.right == null))
            {
                Tnode temp = null;
                if (temp == node.left)
                    temp = node.right;
                else temp = node.left;

                if (temp == null)
                {
                    temp = node;
                    node = null;
                }
                else {
                    node = temp;
                    temp.parent = node.parent;
                }
            }
            // 2 children
            else {
                Tnode temp = node.right;
                while (temp.left != null)
                    temp = temp.left;
                node.value = temp.value;
                node.right = delete(temp.value, node.right, node);
            }
        }
        if (node == null)
            return node;


        //Perform rotation
        int balance = getBalance(node);

        //Left left case
        if (balance > 1 && getBalance(node.left) >= 0)
            node = this.rightRotate(node);
        //Right right case
        if (balance < -1 && getBalance(node.right) <= 0)
            node = this.leftRotate(node);
        //Left right case
        if (balance > 1 && getBalance(node.left) < 0) {
            node.left = this.leftRotate(node.left);
            node = rightRotate(node);
        }
        //Right left case
        if (balance < -1 && getBalance(node.right) > 0) {
            node.right = this.rightRotate(node.right);
            node = leftRotate(node);
        }
        return node;
    }

    public void delete(int value)
    {
        this.root = this.delete(value, this.root,null);
        this.updateDestination();
        this.traversal();
    }

    public Tnode search(int value)
    {
        if (this.root == null) return null;
        Queue<Tnode> frontier = new LinkedList<Tnode>();
        frontier.add(root);
        //System.out.println(frontier.size());
        while (true)
        {
            //System.out.println(frontier.size());
            if (frontier.size() == 0) {
                //System.out.println("Frontier is empty");
                return null;
            }
            else {
                Tnode current = frontier.remove();
                if (current.value == value)
                    return current;
                else {
                    if (current.left != null) frontier.add(current.left);
                    if (current.right != null) frontier.add(current.right);
                }
            }
        }
    }

    public Tnode rightRotate(Tnode y)
    {
        Tnode x = y.left;
        Tnode T2 = x.right;

        x.parent = y.parent;
        y.parent = x;
        if (T2 != null)
            T2.parent = y;

        x.right = y;
        y.left = T2;
        return x;
    }

    public Tnode leftRotate(Tnode y)
    {
        Tnode x = y.right;
        Tnode T2 = x.left;

        x.parent = y.parent;
        y.parent = x;
        if (T2 != null)
            T2.parent = y;

        x.left = y;
        y.right = T2;
        return x;
    }

    public ArrayList<Tnode> successor(Tnode y)
    {
        ArrayList<Tnode> a = new ArrayList<Tnode>();
        if (y == null)
            return a;
        a.add(y);
        a.addAll(successor(y.left));
        a.addAll(successor(y.right));
        return a;
    }

    public ArrayList<Tnode> successor()
    {
        return successor(this.root);
    }

    public void traversal(Tnode t)
    {
        if (t == null)
            return;
        System.out.format("%d %f %f\n",t.value, t.x, t.y);
        traversal(t.left);
        traversal(t.right);

    }

    public void traversal()
    {
        //this.root = updateDestination();
        this.traversal(this.root);
    }

    public Tnode updateDestination()
    {
        if (this.root == null) return null;
        Queue<Tnode> frontier = new LinkedList<Tnode>();
        frontier.add(this.root);
        //System.out.println(frontier.size());
        while (true)
        {
            //System.out.println(frontier.size());
            if (frontier.size() == 0) {
                //System.out.println("Frontier is empty");
                return this.root;
            }
            else {
                Tnode current = frontier.remove();
                double newX = 0;
                if (current == this.root)
                {
                    current.setDestination(400, 300);
                    System.out.println("PASS");
                    //return x;
                }
                else if (current.parent == this.root) {
                    if (current.value < current.parent.value)
                        newX = 200;
                    else
                        newX = 600;
                    current.setDestination(newX, current.parent.y + 120);
                }
                else {
                    if (current.value < current.parent.value) {
                        newX = current.parent.x -
                                Math.abs(current.parent.x - current.parent.parent.x) / 2;
                    }
                    else {
                        newX = current.parent.x +
                                Math.abs(current.parent.x - current.parent.parent.x) / 2;
                    }
                    current.setDestination(newX, current.parent.y + 120);
                }
                if (current.left != null) frontier.add(current.left);
                if (current.right != null) frontier.add(current.right);
            }
        }
    }
}