package Game;

public class ProcessClick {
    public int isPop, isPush, logoChoice;
    double x,y;

    public ProcessClick(double clickedX, double clickedY){
        this.x= clickedX;
        this.y= clickedY;
    }
    public ProcessClick(double clickedX, double clickedY, int logoChoice){
        this.x= clickedX;
        this.y= clickedY;
        this.logoChoice = logoChoice;
    }

    public void stackPopOrPush(){
        if (y>=700 && y<=770) {
            if (x>=300 && x<=470) isPop=1;
            if (x>=500 && x<=670) isPush=1;
        }
    }

    public void linkListPopOrPush(){
        if (y>=700 && y<=870) {
            if (x>=420 && x<=690) isPush=1;
        }

        if (y >= 405 && y <= 462) {
            // Some mathematics code to find out which ball is clicked
            double lower = Math.floor((x - 60) / 100);
            double min = lower * 100 + 60;
            double max = min + 55;
            if ((x - min) * (x - max) <= 0) isPop = (int) lower;
            else isPop = 0;

            /*
            if (x >= 160 && x <= 215) isPop = 1;
            if (x >= 260 && x <= 315) isPop = 2;
            if (x >= 360 && x <= 415) isPop = 3;
            if (x >= 460 && x <= 515) isPop = 4;
            if (x >= 560 && x <= 615) isPop = 5;
             */
        }

    }
    public void treePopOrPush(){
        if (y>=100 && y<=170) {
            if (x>=500 && x<=570) isPush=1;
        }

        if (y >= 100 && y <= 170) {
            if (x>=300 && x<=370) isPop=1;
        }
    }

    public int logoChoice(){
        if (x>=250 && x<=320 && y>=700 && y<=770) return 0;
        if (x>400 && x<=470 && y>=700 && y<=770) return 1;
        if (x>=550 && x<=620 && y>=700 && y<=770) return 2;
        return -1;
    }
}
