package Game;
import tklibs.SpriteUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;


import static Game.Program.clickFirst;
import static Game.Program.p;


public class TreePanel extends JPanel { // not a normal tree, it's a balanced tree

    public Background background;
    public double fps;
    public Node node,node1,node2;
    public Tree n;
    int frameDelay = 110;
    int count = -1;          //just use for tesing rotation
    int isDoing =0;
    Random rnd;
    boolean refactor = false;

    BufferedImage logoImage;
    BufferedImage cover;
    BufferedImage cover2;
    BufferedImage popLogo;
    BufferedImage pushLogo;


    public TreePanel() {
        background = new Background();
        rnd = new Random();
        n = new Tree();  // Create an empty list of node
    }
    static Font fontStart = new Font("Verdana", Font.BOLD, 20);
    /**
     * auto been called by program
     *
     * @param g
     */
    @Override
    public void paintComponent(Graphics g) { // paint cai intro
        super.paintComponent(g);
        if (clickFirst <1) {
            background.render(g);
            logoImage = SpriteUtils.loadImage("assets/images/football/logo.png");
            g.drawImage(logoImage, 150, 200, null);
            cover = SpriteUtils.loadImage("assets/images/football/cover.png");
            g.drawImage(cover,450, 300, null  );
            cover2 = SpriteUtils.loadImage("assets/images/football/cover2.png");
            g.drawImage(cover2,30, 300, null  );
            g.setFont(fontStart);
            g.setColor(Color.white);
            g.drawString( " Click anywhere to start !!! ", 305, 860);

        } else {
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, Settings.GAME_WIDTH, Settings.GAME_HEIGHT);

            this.runAll();
//        background.render(g);
//        player.render(g);
//            System.out.println("painting ");
            GameObject.renderAll(g);
            this.drawMenu(g);
        }
    }

    static Font font = new Font("Verdana", Font.BOLD, 32);
    private void drawMenu(Graphics g) {
//        g.setColor(Color.BLACK);
//        g.fillRect(Settings.BACKGROUND_WIDTH,0,Settings.GAME_WIDTH - Settings.BACKGROUND_WIDTH, Settings.GAME_HEIGHT);
        g.setColor(Color.RED);
        g.drawString(fps + " ",750, 50);
        logoImage = SpriteUtils.loadImage("assets/images/football/logo.png");
        g.drawImage(logoImage, 150, 10, null);
        popLogo = SpriteUtils.loadImage("assets/images/football/shoe.png");
        g.drawImage(popLogo,300, 100,null);
        pushLogo = SpriteUtils.loadImage("assets/images/football/gloves.png");
        g.drawImage(pushLogo,500, 100,null);
        g.setFont(fontStart);
        for (Tnode t: n.successor()) {
            g.setFont(font);
            g.setColor(Color.red);
            g.drawString( t.value+"", (int)t.position.x+20, (int)t.position.y+41);
//            g.drawString("dcm test chu", 50, 500);
        }

        g.setColor(Color.blue);
        for (Tnode t: n.successor())
        {
            if (t.parent != null)
                if (t.value > t.parent.value)
                    g.drawLine((int) t.parent.position.x + 55, (int) t.parent.position.y + 57,
                        (int) t.position.x + 35, (int) t.position.y + 2);
                else
                    g.drawLine((int) t.parent.position.x + 15, (int) t.parent.position.y + 55,
                            (int) t.position.x + 35, (int) t.position.y + 5);
        }
    }

    int frameCount=0;
    public void runAll() {
        // not necessary to make all objects run
        GameObject.runAll();


        // Add a new node
        if (p!=null && p.isPush==1) { // glove
            addNodeAnimation(count);
        }


        //Delete a node
        if (p!= null && p.isPop == 1) { //shoe
            deleteNodeAnimation(count);
        }
        if (refactor)
        {
            refactor = false;
            for (Tnode t: n.successor())
            {
                t.moveTo(t.x, t.y);
                if (t.position.x != t.x || t.position.y != t.y)
                    refactor = true;
            }
        }
    }

    public void gameLoop() {
        //long ~ big int
        long lastTime = System.currentTimeMillis();

        while (true) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - lastTime >= 1000 / 60) {
                fps = 1000/ (currentTime - lastTime);
                //paint
                this.repaint();
                //runt all logic  ()
//                this.runAll();
//                System.out.println();
                lastTime = currentTime;
            }

        }
    }

    public void addNodeAnimation(int value)
    {
        System.out.println(value);
        if (isDoing==0) {

            n.add(value);
            isDoing=1;
        }

        Tnode node = n.search(value);
        if (node == null) return;
        for (Tnode t : n.successor())
        {
            t.moveTo(t.x, t.y);
        }

        if (node.position.x == node.x && node.position.y== node.y) {
            p.isPush=0;
            isDoing=0;
            //count = rnd.nextInt();  //just for debugging
            //count++;                    //just for debugging
            System.out.println("OK");
        }
    }

    public void deleteNodeAnimation(int value)
    {
        Tnode node = n.search(value);
        if (node == null)
            return;
        //node.moveTo(-100, -100);
        Tnode temp;
        if (node.left == null || node.right == null)
        {
            temp = node.left;
            if (node.left == null)
                temp = node.right;
        }
        else {
            temp = node.right;
            while (temp.left != null)
                temp = temp.left;
        }
        //No child
        if (temp == null)
        {
            node.position.x = -100;
            node.position.y = -100;
            return;
        }
        //Has children
        if (temp != null) {
            node.position.x = -100;
            node.position.y = -100;
            temp.moveTo(node.x, node.y);
        }


        if (temp.position.x == node.x && temp.position.y == node.y)
        {
            p.isPop=0;
            isDoing = 0;
            n.delete(value);
            //System.out.format("%d %f %f", node.value, node.position.x, node.position.y);
            node.position.x = node.x;
            node.position.y = node.y;
            //temp.position.x = -100;
            //temp.position.y = -100;
            refactor = true;
        }
    }
}

