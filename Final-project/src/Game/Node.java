package Game;

import Game.Renderer.Renderer;
import tklibs.SpriteUtils;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Node extends GameObject {
    public Node(double x,double y, int value){
        this.value = value;
        BufferedImage image = SpriteUtils.loadImage("assets/images/football/ball.png");
        renderer = new Renderer("assets/images/football/ball.png",8, false);
        position.set(x,y);
    }

    public Node(double x,double y){
        BufferedImage image = SpriteUtils.loadImage("assets/images/football/ball.png");
        renderer = new Renderer("assets/images/football/ball.png",8, false);
        position.set(x,y);
    }
    public void NodeMoveTo(double xTo, double yTo){
        if (this.position.y!= yTo) {
            if (this.position.y>yTo) this.velocity.set(0,-10); // huong di chuyen
            else this.velocity.set(0,10);
        }
        else {
            this.velocity.set(0,0);
            if (this.position.x!=xTo) {
                if (this.position.x>xTo) this.velocity.set(-10,0); // huong di chuyen
                else this.velocity.set(10,0);
            }
            else this.velocity.set(0,0);
        }
    }

    public void moveToX(double xTo, double yPrev){
        double yTo= yPrev+100;
//        if (this.position.y!= yTo) this.velocity.set(0,5);
//        else {
//            this.velocity.set(0,0);
//            if (this.position.x!=xTo) this.velocity.set(5,0);
//            else this.velocity.set(0,0);
//        }
//        if (this.position.x==xTo) {
//            if (this.position.y!= yPrev) this.velocity.set(0,5);
//        }
        this.NodeMoveTo(xTo, yPrev+100);
//        System.out.println(this.position.y + "/" + yPrev+200 );
        if (this.position.x==xTo) this.NodeMoveTo(xTo, yPrev);
    }

    @Override
    public void run() {
        super.run();
    }

}
