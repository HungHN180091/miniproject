package Game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

//import Game.Map.Map;
//
//import static Game.Map.Map.tiles;


public class Program {
    public static JFrame window = new JFrame();
    public static double clickedX, clickedY;
    public static int clickCount;
    public static int clickFirst;
    public static JPanel startPanel = new JPanel();
    public static StackPanel stackPanel = new StackPanel();
    public static LinkListPanel linklistPanel = new LinkListPanel();
    public static TreePanel treePanel = new TreePanel();
    public static ProcessClick p;
    public static int logoChoice=-1;
    public static int node,value,count=4;

    public static void drawStack(){
        JLabel label = new JLabel("Enter the value:");
        JButton button = new JButton("OK");
        JTextField field = new JTextField(5);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                value = Integer.parseInt(field.getText());
                System.out.println("in ra: "+value);
                if(count>-1) stackPanel.n[count].value=value;
                count--;
            }
        });
        stackPanel.add(label);
        stackPanel.add(field);
        stackPanel.add(button);
        stackPanel.setPreferredSize(new Dimension(Settings.GAME_WIDTH, Settings.GAME_HEIGHT));
        window.add(stackPanel);
    }

    public static void drawLinkList(){
        System.out.println("drawing ll");
        JLabel valueLabel = new JLabel("Enter the value :");
        JLabel nodeLabel = new JLabel("Enter the node:");
        JButton button = new JButton("OK");
        JTextField valueField = new JTextField(5);
        JTextField nodeField = new JTextField(5);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                value = Integer.parseInt(valueField.getText());
                node = Integer.parseInt(nodeField.getText());
                System.out.println("ia ra: "+value);
                linklistPanel.n.get(node).value=value;
                count++;
            }
        });
        linklistPanel.add(nodeLabel);
        linklistPanel.add(nodeField);
        linklistPanel.add(valueLabel);
        linklistPanel.add(valueField);
        linklistPanel.add(button);
        linklistPanel.setPreferredSize(new Dimension(Settings.GAME_WIDTH, Settings.GAME_HEIGHT));
        window.add(linklistPanel);
    }

    public static void drawTree(){
        JLabel label = new JLabel("Enter the value:");
        JButton button = new JButton("OK");
        JTextField field = new JTextField(5);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                value = Integer.parseInt(field.getText());
                System.out.println("in ra: "+value);
                treePanel.count = value;
                count--;
            }
        });
        treePanel.add(label);
        treePanel.add(field);
        treePanel.add(button);
        treePanel.setPreferredSize(new Dimension(Settings.GAME_WIDTH, Settings.GAME_HEIGHT));
        window.add(treePanel);
    }

    public static void main(String[] args) {
        MouseAdapter mouseHandler = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                clickFirst++;
                clickedX = e.getX() - window.getInsets().left;
                clickedY = e.getY() - window.getInsets().top;
//                System.out.println((int) ((clickedX - 2.2) / TILES_WIDTH) + "  " + (int) ((clickedY - 2) / TILES_HEIGHT)); // in tiles
                System.out.println(clickedX+ " " + clickedY);
                if (clickFirst <=1) {
                    p = new ProcessClick(clickedX, clickedY);
//                    logoChoice = p.logoChoice();
//                    if (logoChoice<0) clickFirst--;
                    System.out.println("choice = "+ logoChoice);

                }
                if (clickFirst > 1 ) {
                    clickCount++;
                    p = new ProcessClick(clickedX, clickedY);
                    if(logoChoice==0) p.stackPopOrPush();
                    if(logoChoice==1) p.linkListPopOrPush();
                    if(logoChoice==2) p.treePopOrPush();

//                    SelectAndSwap s = new SelectAndSwap((int) ((clickedX - 2.2 * TILES_WIDTH) / TILES_WIDTH), (int) ((clickedY - 2.5 * TILES_HEIGHT) / TILES_HEIGHT), clickCount);
//                    s.collectClickedValue();
                }
            }
        };
        window.addMouseListener(mouseHandler);
//        window.addKeyListener(keyHandler);

        System.out.println(System.currentTimeMillis());
//        JFrame window = new JFrame();
        window.setTitle("Group 3 - 20192 OOP Mini Project");
        System.out.println("choice == "+logoChoice);
//        JLabel label = new JLabel("Nhap app:");
//        JButton button = new JButton("OK");
//        JTextField field = new JTextField(5);
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                logoChoice = Integer.parseInt(field.getText());
//                System.out.println("in ra logoChoice: "+ logoChoice);
//            }
//        });
//        startPanel.setLayout(new BoxLayout(startPanel, BoxLayout.Y_AXIS));
        JLabel label = new JLabel("Please choose the data structure you want:");
        JButton button0 = new JButton("Stack");
        JButton button1 = new JButton("Linked List");
        JButton button2 = new JButton("Balanced tree");
        button0.setPreferredSize(new Dimension(100,50));
        button1.setPreferredSize(new Dimension(100,50));
        button2.setPreferredSize(new Dimension(100,50));
        JTextField field = new JTextField(5);
        button0.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logoChoice = 0;
                System.out.println("in ra logoChoice: "+ logoChoice);
            }
        });
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logoChoice = 1;
                System.out.println("in ra logoChoice: "+ logoChoice);
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logoChoice = 2;
                System.out.println("in ra logoChoice: "+ logoChoice);
            }
        });
//        startPanel.add(label);
//        startPanel.add(field);
        startPanel.add(button0);
        startPanel.add(button1);
        startPanel.add(button2);
        startPanel.setPreferredSize(new Dimension(Settings.GAME_WIDTH, Settings.GAME_HEIGHT));
        window.add(startPanel);
        window.setVisible(true);
        window.setResizable(true);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.pack();
        while (logoChoice==-1) System.out.println(-1);
        window.getContentPane().removeAll();
        if (logoChoice==0) drawStack();
        if (logoChoice==1) drawLinkList();
        if (logoChoice==2)  drawTree();
        window.setResizable(true);
//        window.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //JPanel panel = new JPanel();

        //GamePanel panel = new GamePanel();

        window.pack();
        //panel.setBackground(Color.CYAN);

        // bat su kien phim
        KeyAdapter keyHandler = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_W) {
                    KeyEventPress.isUpPress = true;
                }

                if (e.getKeyCode() == KeyEvent.VK_D) {
                    KeyEventPress.isRightPress = true;
                }

                if (e.getKeyCode() == KeyEvent.VK_S) {
                    KeyEventPress.isDownPress = true;
                }
                if (e.getKeyCode() == KeyEvent.VK_A) {
                    KeyEventPress.isLeftPress = true;
                }
                if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                    KeyEventPress.isSelectPress = true;
                }
            }


            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_W) {
                    KeyEventPress.isUpPress = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_D) {
                    KeyEventPress.isRightPress = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_S) {
                    KeyEventPress.isDownPress = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_A) {
                    KeyEventPress.isLeftPress = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                    KeyEventPress.isSelectPress = false;
                }
            }
        };

        // clickCount is odd <=> selected the first tile
        // clickCount is even <=> selected the second tile

        window.setVisible(true);
        // start game
        if (logoChoice==0) stackPanel.gameLoop();
        if (logoChoice==1) linklistPanel.gameLoop();
        if (logoChoice==2) treePanel.gameLoop();
    }
}


