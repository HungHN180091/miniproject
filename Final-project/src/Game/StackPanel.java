package Game;
import tklibs.SpriteUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

import static Game.Program.*;


public class StackPanel extends JPanel {
    // duong dan tuyet doi: /Users/ngando1720/Documents/Java OOP/ci-begin-master/assets/images/players/straight/0.png
    // duong dan tuong doi: assets/images/players/straight/0.png

    public Background background;
    public double fps;
    public Node node,node1,node2;
    public Node[] n= new Node[5];
    int frameDelay = 110;
    int count=0;
    int done=0;
    int isDoing =0;
    int i;

    BufferedImage logoImage, pushLogo, popLogo, cover, cover2, ballLogo, shoeLogo, gloveLogo;

    public StackPanel() {
        background = new Background();
//        node = new Node(-20,-20);
//        node1 = new Node(400,300);
//        node2= new Node(400,400);
        n[0]=new Node(-50,-50);
        n[1]=new Node(-50,-50);
        n[2]=new Node(-50,-50);
        n[3]= new Node(-50,-50); //410 340 270
        n[4]= new Node(-50,-50);
//        Enemy enemy = new Enemy()

//        DetectMatch.detect();
    }
    private static Font fontStart = new Font("Verdana", Font.BOLD, 20);
    /**
     * auto been called by program
     *
     * @param g
     */
    @Override
    public void paintComponent(Graphics g) { // paint cai intro
        super.paintComponent(g);
        if (clickFirst <1) {
            background.render(g);
            logoImage = SpriteUtils.loadImage("assets/images/football/logo.png");
            g.drawImage(logoImage, 150, 50, null);
            cover = SpriteUtils.loadImage("assets/images/football/cover.png");
            g.drawImage(cover,450, 150, null  );
            cover2 = SpriteUtils.loadImage("assets/images/football/cover2.png");
            g.drawImage(cover2,30, 150, null  );
//            ballLogo = SpriteUtils.loadImage("assets/images/football/ball.png");
//            g.drawImage(ballLogo, 250, 700, null);
//            shoeLogo = SpriteUtils.loadImage("assets/images/football/shoe.png");
//            g.drawImage(shoeLogo, 400, 700, null);
//            gloveLogo = SpriteUtils.loadImage("assets/images/football/gloves.png");
//            g.drawImage(gloveLogo, 550, 700, null);

            g.setFont(fontStart);
            g.setColor(Color.white);
            g.drawString( " Click anywhere to start ", 305, 860);

        } else {
//            System.out.println("choice = "+logoChoice);
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, Settings.GAME_WIDTH, Settings.GAME_HEIGHT);

            this.runAll();
//        background.render(g);
//        player.render(g);
//            System.out.println("painting ");
            GameObject.renderAll(g);
            this.drawMenu(g);
        }
    }

    static Font font = new Font("Verdana", Font.BOLD, 32);
    private void drawMenu(Graphics g) {
//        g.setColor(Color.BLACK);
//        g.fillRect(Settings.BACKGROUND_WIDTH,0,Settings.GAME_WIDTH - Settings.BACKGROUND_WIDTH, Settings.GAME_HEIGHT);
//        g.setColor(Color.RED);
//        g.drawString(fps + " ",750, 50);
        logoImage = SpriteUtils.loadImage("assets/images/football/logo.png");
        g.drawImage(logoImage, 150, 60, null);
        popLogo = SpriteUtils.loadImage("assets/images/football/poplogo.png");
        g.drawImage(popLogo,300, 700,null);
        pushLogo = SpriteUtils.loadImage("assets/images/football/pushlogo.png");
        g.drawImage(pushLogo,500, 700,null);

        for (int j = 0; j < 5; j++) {
            g.setFont(font);
            g.setColor(Color.red);
            g.drawString( n[j].value+"", (int)n[j].position.x+20, (int)n[j].position.y+41);
//            g.drawString("dcm test chu", 50, 500);
        }
    }

    int frameCount=0;
    public void runAll() {
        // not necessary to make all objects run
        GameObject.runAll();
        if (done %2==0 && p!=null && p.isPush==1) { // glove
            if (isDoing==0) {
                i=4;
                while (n[i].position.x>0 && n[i].position.y>0)  i--;
                isDoing=1;
            }
            n[i].moveTo(400,270+i*70);
            if (n[i].position.x==400 && n[i].position.y==270+i*70) {
                p.isPush=0;
                isDoing=0;
            }
        }
        count++;
        if (done %2==0 && count >=120 && p!= null && p.isPop==1) { //shoe
//            i=0;
//            while (n[i].position.x<0 && n[i].position.y<0)  i++;
//            n[i].moveTo(-80,-80);
//            if (n[i].position.x==-80 && n[i].position.y==-80) p.isPop=0;
            if (isDoing==0) {
                i=0;
                while (n[i].position.x<0 && n[i].position.y<0)  i++;
                isDoing=1;
            }
            n[i].moveTo(-60,-60);
            if (n[i].position.x==-60 && n[i].position.y==-60) {
                p.isPop=0;
                isDoing=0;
            }
        }


//        System.out.println("value= " + n[0].value + n[1].value + n[2].value + n[3].value + n[4].value );
    }

    public void gameLoop() {
        //long ~ big int
        long lastTime = System.currentTimeMillis();

        while (true) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - lastTime >= 1000 / 60) {
                fps = 1000/ (currentTime - lastTime);
                //paint
                this.repaint();
                //runt all logic  ()
//                this.runAll();
//                System.out.println();
                lastTime = currentTime;
            }

        }
    }
}

