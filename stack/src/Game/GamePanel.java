package Game;
import tklibs.SpriteUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;


import static Game.Program.clickFirst;
import static Game.Program.p;


public class GamePanel extends JPanel {
    // duong dan tuyet doi: /Users/ngando1720/Documents/Java OOP/ci-begin-master/assets/images/players/straight/0.png
    // duong dan tuong doi: assets/images/players/straight/0.png

    public Background background;
    public double fps;
    public Node node,node1,node2;
    public Node[] n= new Node[5];
    int frameDelay = 110;
    int count=0;
    int done=0;

    BufferedImage logoImage;
    BufferedImage cover;
    BufferedImage cover2;
    BufferedImage popLogo;
    BufferedImage pushLogo;

    public GamePanel() {
        background = new Background();
        node = new Node(80,80); n[0]=new Node(-1,-1);
        node1 = new Node(400,300); n[1]= new Node(400,300);
        node2= new Node(400,400); n[2]= new Node(400,400);
//        Enemy enemy = new Enemy()

//        DetectMatch.detect();
    }
    static Font fontStart = new Font("Verdana", Font.BOLD, 20);
    /**
     * auto been called by program
     *
     * @param g
     */
    @Override
    public void paintComponent(Graphics g) { // paint cai intro
        super.paintComponent(g);
        if (clickFirst <1) {
            background.render(g);
            logoImage = SpriteUtils.loadImage("assets/images/football/logo.png");
            g.drawImage(logoImage, 150, 200, null);
            cover = SpriteUtils.loadImage("assets/images/football/cover.png");
            g.drawImage(cover,450, 300, null  );
            cover2 = SpriteUtils.loadImage("assets/images/football/cover2.png");
            g.drawImage(cover2,30, 300, null  );
            g.setFont(fontStart);
            g.setColor(Color.white);
            g.drawString( " Click anywhere to start !!! ", 305, 860);

        } else {
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, Settings.GAME_WIDTH, Settings.GAME_HEIGHT);
//            popLogo = SpriteUtils.loadImage("assets/images/football/shoe.png");
//            g.drawImage(popLogo,300, 700,null);
//            pushLogo = SpriteUtils.loadImage("assets/images/football/gloves.png"); // chua ve dc, todo
//            g.drawImage(pushLogo,500, 700,null);
            this.runAll();
//        background.render(g);
//        player.render(g);
//            System.out.println("painting ");
            GameObject.renderAll(g);
            this.drawMenu(g);
        }
    }

    static Font font = new Font("Verdana", Font.BOLD, 46);
    private void drawMenu(Graphics g) {
//        g.setColor(Color.BLACK);
//        g.fillRect(Settings.BACKGROUND_WIDTH,0,Settings.GAME_WIDTH - Settings.BACKGROUND_WIDTH, Settings.GAME_HEIGHT);
        g.setColor(Color.RED);
        g.drawString(fps + " ",750, 50);
        logoImage = SpriteUtils.loadImage("assets/images/football/logo.png");
        g.drawImage(logoImage, 150, 10, null);
        popLogo = SpriteUtils.loadImage("assets/images/football/shoe.png");
        g.drawImage(popLogo,300, 700,null);
        pushLogo = SpriteUtils.loadImage("assets/images/football/gloves.png"); // chua ve dc, todo
        g.drawImage(pushLogo,500, 700,null);
    }

    int frameCount=0;
    public void runAll() {
        // not necessary to make all objects run
        GameObject.runAll();

        if (done %2==0 && p!=null && p.isPush==1) {
            node.moveTo(400,200);
            if (node.position.x==400 && node.position.y==200) done=1;
        }
        count++;
        if (done %2==1 && count >=120 && p!= null && p.isPop==1) {
            int i;
//            while (n[i].position.x<0 && n[i].position.y<0) // todo
            node.moveTo(-100,0);
            if (node.position.x==0 && node.position.y==0) done=2;
        }

//        System.out.println("x=" + node.position.x);
    }

    public void gameLoop() {
        //long ~ big int
        long lastTime = System.currentTimeMillis();

        while (true) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - lastTime >= 1000 / 60) {
                fps = 1000/ (currentTime - lastTime);
                //paint
                this.repaint();
                //runt all logic  ()
//                this.runAll();
//                System.out.println();
                lastTime = currentTime;
            }

        }
    }
}

